<?php
namespace LettingHQ\LettingHQ\Classes\LettingHQ;

use LettingHQ\LettingHQ\Classes\LettingHQ\Base;

class Checklist extends Base {
	public function getChecklists($data) {
		$this->setConfig('route', 'checklists');

		return $this->request($this->buildRequest('GET', $data));
	}

	public function getChecklist($data) {
		$this->setConfig('route', 'checklists');

		return $this->request($this->buildRequest('GET', $data));
	}

	public function getPublicChecklist($data) {
		$this->setConfig('route', 'public/checklists');

		return $this->request($this->buildRequest('GET', $data));
	}
}
