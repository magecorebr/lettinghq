<?php
namespace LettingHQ\LettingHQ\Classes\LettingHQ;

use LettingHQ\LettingHQ\Classes\LettingHQ\Base;

class ChecklistInstance extends Base {
	public function getChecklistInstance($data) {
		$this->setConfig('route', 'checklist_instances');

		return $this->request($this->buildRequest('GET', $data));
	}

	public function postChecklistInstance($data) {
		$this->setConfig('route', 'checklist_instances');

		return $this->request($this->buildRequest('POST', $data));
	}
}
