<?php
namespace LettingHQ\LettingHQ\Classes\LettingHQ;

use LettingHQ\LettingHQ\Classes\LettingHQBase;

class Base extends LettingHQBase {
	public function __construct() {
		parent::__construct();
		
		$this->setConfig('endpoint', config('lettinghq.endpoints.lhq', 'https://api.lettinghq.com'));
		$this->setApi();
	}
}
