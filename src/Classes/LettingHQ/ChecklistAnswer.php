<?php
namespace LettingHQ\LettingHQ\Classes\LettingHQ;

use LettingHQ\LettingHQ\Classes\LettingHQ\Base;

class ChecklistAnswer extends Base {
	public function postChecklistAnswers($data) {
		$this->setConfig('route', 'checklist_answers');

		return $this->request($this->buildRequest('POST', $data));
	}
}
