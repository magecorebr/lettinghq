<?php
namespace LettingHQ\LettingHQ\Classes;

use LettingHQ\LettingHQ\Interfaces\LettingHQInferface;

use Exception;
use GuzzleHttp;

class LettingHQBase {
    protected $config;
    protected $current;
    protected $classes;

    public function __construct($email = null, $authToken = null, $apiKey = null) {
        // Set instance from config file
        $this->setConfig('apiKey',    config('lettinghq.user.api_key', null));
        $this->setConfig('email',     config('lettinghq.user.email', null));
        $this->setConfig('authToken', config('lettinghq.user.auth_token', null));
        $this->setConfig('version',   config('lettinghq.version', '1'));

        // Overwrite configs with variables if they are set
        if ($email !== null) {
            $this->setConfig('email', $email);
        }

        if ($authToken !== null) {
            $this->setConfig('authToken', $authToken);
        }

        if ($apiKey !== null) {
            $this->setConfig('apiKey', $apiKey);
        }

        // Throw an error if a config is still missing
        if (!$this->getConfig('apiKey') || !$this->getConfig('email') || !$this->getConfig('authToken')) {
            $this->error('User configuration missing');
        }
    }

    public function setConfig($key, $config) {
        $this->config[$key] = $config;
    }

    public function getConfig($config) {
        return isset($this->config[$config]) ? $this->config[$config] : false;
    }

    public function changeCategory($category) {
        if (!array_key_exists($category, $this->classes)) {
            $this->error('Unable to find category');
        }

        $this->current = $this->classes[$category];
    }

    public function setApi() {
        if (!$this->getConfig('endpoint') || !$this->getConfig('version')) {
            $this->error('API configuration missing');
        }

        $this->setConfig('api', rtrim($this->config['endpoint'], '/') . '/v' . $this->config['version'] . '/');
    }

    public function buildRequest($type, $data = null) {
        $request = ['url' => $this->getConfig('api') . $this->getConfig('route'), 'type' => $type];

        if ($data) {
            if ($type === 'GET') {
                if (isset($data['id'])) {
                    $request['url'] .= '/' . $data['id'];
                    unset($data['id']);
                }

                if (count($data) > 0) {
                    $request['query'] = $data;
                }
            } else {
                $request['data'] = $data;
            }
        }

        return $request;
    }

    public function request($request) {
        $client             = new GuzzleHttp\Client();
        $params             = [
            'exceptions'    => false,
            'headers'       => [
                'Content-Type'  => 'application/json',
                'email'         => $this->getConfig('email'),
                'token'         => $this->getConfig('authToken'),
                'api-key'       => $this->getConfig('apiKey')
            ],
        ];

        if (!isset($request['url'])) {
            $this->error('No URL set');
        } else {
            $url = $request['url'];
        }

        if (!isset($request['type'])) {
            $this->error('No request type set');
        } else {
            $type = strtoupper($request['type']);
        }

        if (isset($request['data'])) {
            $params['form_params'] = $request['data'];
        }

        if (isset($request['query'])) {
            $params['query'] = $request['query'];
        }

        $response = $client->request($type, $url, $params);

        return $this->buildResponse($response);
    }

    public function buildResponse($response) {
        $responseCode = $response->getStatusCode();
        $responseBody = json_decode($response->getBody(), true);

        if ($responseCode != '200') {
            $this->error($responseBody['response']);
        } else {
            return $this->success($responseBody['response']);
        }
    }

    public function success($message) {
        return $message;
    }

    public function error($message) {
        throw new Exception($message);
    }
}
