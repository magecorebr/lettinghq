<?php
namespace LettingHQ\LettingHQ;

use LettingHQ\LettingHQ\Classes\LettingHQBase;
use LettingHQ\LettingHQ\Classes\LettingHQ\Checklist;
use LettingHQ\LettingHQ\Classes\LettingHQ\ChecklistInstance;
use LettingHQ\LettingHQ\Classes\LettingHQ\ChecklistAnswer;

class LettingHQ extends LettingHQBase {
    protected $classes;

    public function __construct($category) {
        // Define all sub classes
        $this->classes['checklist']             = new Checklist();
        $this->classes['checklist_instance']    = new ChecklistInstance();
        $this->classes['checklist_answer']      = new ChecklistAnswer();

        // Throw an error if the class trying to be set does not exist
        if (!array_key_exists($category, $this->classes)) {
            $this->error('Unable to find category');
        }

        $this->current = $this->classes[$category];
    }

    public function request($method, $payload = null) {
        return $this->current->$method($payload);
    }
}
