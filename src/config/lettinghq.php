<?php
return [
    'version' => env('LETTINGHQ_VERSION', '1'),
    'user'    => [
        'api_key'    => env('LETTINGHQ_API_KEY', 'api-key'),
        'email'      => env('LETTINGHQ_EMAIL', 'email'),
        'auth_token' => env('LETTINGHQ_AUTH_TOKEN', 'auth-token'),
    ],
    'endpoints' => [
        'lhq'   => env('LETTINGHQ_ENDPOINTS_LHQ', 'https://api.lettinghq.com'),
        'lc'    => env('LETTINGHQ_ENDPOINTS_LC',  'https://api.lettingcheck.com'),
        'lf'    => env('LETTINGHQ_ENDPOINTS_LF',  'https://api.lettingfix.com'),
    ],
];
